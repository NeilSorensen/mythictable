# DataAccessLib Quick Tutorial

## Structure and Responsibilities
- **Database:** The database is responsible for storing and organizing data in an efficient manner. The database may return data in a blob, generic object, or string format, like json or XML.

- **Data Access Object:** The DAO is responsible for interfacing, and querying the database. We use a DAO to control how the DB is accessed.

- **Data Provider:** The data provider is reponsible for interacing the DAO to the rest of the program's POCO objects. When the program needs to retrieve or store POCO objects, a data provider should be requested.

## How to Call a DataProvider<T> with a DI Constructor
Consider the object type needed is `Dollar`, we need a `IDataProvider<Dollar>`. Inside the body of the constructor, a data provider can be retrieved like this:
```csharp
public class MoneyChanger
{
	IDataProvider<Dollar> _dollarProvider;

	public MoneyChanger(IServiceProvider serviceProvider)
	{
		_dollarProvider = (IDataProvider<Dollar>)serviceProvider.GetService(typeof(IDataProvider<Dollar>));
	}
}
```

The resulting action flow should look like this:
```mermaid
graph TD
    U[User Interaction] --> A
    A[Server] -->|Requested Data| B[JObjectDataProvider< Dollar >]
    B -->|Request to an IDataAccessObject| C[Mongo2GoDAO]
    C -->|Request to the Database| D[Mongo2Go DB]
    D -->|Database returns raw data| C
    C -->|DAO returns object, or string| B
    B -->|Data Provider returns formatted object| F[DollarDTO]
    F -->|DTO returned| A
```
