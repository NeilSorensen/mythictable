const state = {
    windowSize: '',
    displayingModal: false,
};

const getters = {
    isDisplayingModal(state) {
        return state.displayingModal;
    },
};

const mutations = {
    updateWindowSize(state, windowSize) {
        state.windowSize = windowSize;
    },
    setDisplayingModal(state, isDisplaying) {
        state.displayingModal = isDisplaying;
    },
};

let trackWindowResize;

const actions = {
    windowResize({ commit }) {
        commit('updateWindowSize', window.innerWidth);
    },
    initiateFunctions({ dispatch }) {
        dispatch('windowResize');
        trackWindowResize = () => dispatch('windowResize');
    },
    addTracking({ dispatch }) {
        dispatch('initiateFunctions');
        window.addEventListener('resize', trackWindowResize);
    },
    removeTracking() {
        window.removeEventListener('resize', trackWindowResize);
    },
};

const WindowStore = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};

export default WindowStore;
